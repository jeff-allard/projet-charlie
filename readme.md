# Projet Charlie

Installer nodeJS pour installer les dépendances du projet /!\
Puis lancer la commande `npm install` pour récupérer les modules nodes.

## Lancer le programme

```
npm run build
```

Utile pour la première installation du projet. Va créer le dossier dist/ avec le fichier JS compilé et `index.html`.

```
npm run start
```

Cette commande va rendre disponible le projet sur `localhost:8080`.
