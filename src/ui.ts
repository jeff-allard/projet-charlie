import {
  TextBlock,
  StackPanel,
  AdvancedDynamicTexture,
  Image,
  Button,
  Rectangle,
  Control,
  Grid,
} from "@babylonjs/gui";
import { Scene } from "@babylonjs/core";

export class Hud {
  private _scene: Scene;
  private _pauseMenu: Rectangle;
  private _playerUI: any; //UI Elements

  gamePaused: boolean;
  pauseBtn: Button;

  constructor(scene: Scene) {
    this._scene = scene;
    this._createPauseMenu();

    const playerUI = AdvancedDynamicTexture.CreateFullscreenUI("UI");
    this._playerUI = playerUI;
    this._playerUI.idealHeight = 720;

    const pauseBtn = Button.CreateImageOnlyButton(
      "pauseBtn",
      "./sprites/pauseBtn.png"
    );
    pauseBtn.width = "50px";
    pauseBtn.height = "50px";
    pauseBtn.thickness = 0;
    pauseBtn.verticalAlignment = 0;
    pauseBtn.horizontalAlignment = 1;
    pauseBtn.top = "8px";
    playerUI.addControl(pauseBtn);
    pauseBtn.zIndex = 10;

    this.pauseBtn = pauseBtn;
    //when the button is down, make pause menu visable and add control to it
    pauseBtn.onPointerDownObservable.add(() => {
      this._pauseMenu.isVisible = true;
      playerUI.addControl(this._pauseMenu);
      this.pauseBtn.isHitTestVisible = false;

      //when game is paused, make sure that the next start time is the time it was when paused
      this.gamePaused = true;
    });
  }

  //---- Pause Menu Popup ----
  private _createPauseMenu(): void {
    this.gamePaused = false;

    const pauseMenu = new Rectangle();
    pauseMenu.horizontalAlignment = Control.HORIZONTAL_ALIGNMENT_CENTER;
    pauseMenu.verticalAlignment = Control.VERTICAL_ALIGNMENT_CENTER;
    pauseMenu.height = 0.8;
    pauseMenu.width = 0.5;
    pauseMenu.thickness = 0;
    pauseMenu.isVisible = false;

    //background image
    const image = new Image("pause", "sprites/pause.png");
    pauseMenu.addControl(image);

    //stack panel for the buttons
    const stackPanel = new StackPanel();
    stackPanel.width = 0.83;
    pauseMenu.addControl(stackPanel);

    const resumeBtn = Button.CreateSimpleButton(
      "resume",
      "Reprendre la partie"
    );
    resumeBtn.width = 1;
    resumeBtn.height = "44px";
    resumeBtn.color = "white";
    resumeBtn.fontFamily = "Kanit";
    resumeBtn.paddingBottom = "14px";
    resumeBtn.cornerRadius = 14;
    resumeBtn.fontSize = "18px";
    resumeBtn.textBlock.resizeToFit = true;
    resumeBtn.verticalAlignment = Control.VERTICAL_ALIGNMENT_CENTER;
    stackPanel.addControl(resumeBtn);

    this._pauseMenu = pauseMenu;

    //when the button is down, make menu invisable and remove control of the menu
    resumeBtn.onPointerDownObservable.add(() => {
      this._pauseMenu.isVisible = false;
      this._playerUI.removeControl(pauseMenu);
      this.pauseBtn.isHitTestVisible = true;

      //game unpaused, our time is now reset
      this.gamePaused = false;
    });
  }
}
