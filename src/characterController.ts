import {
  Scene,
  TransformNode,
  Mesh,
  ShadowGenerator,
  Vector3,
  UniversalCamera,
  Quaternion,
  Ray,
} from "@babylonjs/core";
import { PlayerInput } from "./inputController";

export class Player extends TransformNode {
  public camera: UniversalCamera; // ajout de la propriété camera
  public scene: Scene;
  private _input: PlayerInput;

  public mesh: Mesh;

  //Camera
  private _camRoot: TransformNode;
  private _yTilt: TransformNode;

  private _grounded: boolean;
  private _lastGroundPos: Vector3 = Vector3.Zero(); // keep track of the last grounded position
  private _gravity: Vector3 = new Vector3();
  private _jumpCount: number = 1;

  private static readonly ORIGINAL_TILT: Vector3 = new Vector3(
    0.5934119456780721,
    0,
    0
  );
  private static readonly PLAYER_SPEED: number = 0.3;
  private static readonly JUMP_FORCE: number = 0.7;
  private static readonly GRAVITY: number = -5;

  //player movement vars
  private _deltaTime: number = 0;
  private _h: number;
  private _v: number;

  private _moveDirection: Vector3 = new Vector3();
  private _inputAmt: number;

  constructor(assets, scene: Scene, shadowGenerator: ShadowGenerator, input?) {
    super("player", scene);
    this.scene = scene;
    this._setupPlayerCamera();

    this.mesh = assets.mesh;
    this.mesh.parent = this;
    this.scene.getLightByName("sparklight").parent =
      this.scene.getTransformNodeByName("Empty");

    shadowGenerator.addShadowCaster(assets.mesh);

    this._input = input;
  }

  private _setupPlayerCamera(): UniversalCamera {
    //root camera parent that handles positioning of the camera to follow the player
    this._camRoot = new TransformNode("root");
    this._camRoot.position = new Vector3(0, 0, 0); //initialized at (0,0,0)
    //to face the player from behind (180 degrees)
    this._camRoot.rotation = new Vector3(0, Math.PI, 0);

    //rotations along the x-axis (up/down tilting)
    let yTilt = new TransformNode("ytilt");
    //adjustments to camera view to point down at our player
    yTilt.rotation = Player.ORIGINAL_TILT;
    this._yTilt = yTilt;
    yTilt.parent = this._camRoot;

    //our actual camera that's pointing at our root's position
    this.camera = new UniversalCamera(
      "cam",
      new Vector3(0, 0, -30),
      this.scene
    );
    this.camera.lockedTarget = this._camRoot.position;
    this.camera.fov = 0.47350045992678597;
    this.camera.parent = yTilt;

    this.scene.activeCamera = this.camera;
    return this.camera;
  }

  // GAME UPDATES
  private _beforeRenderUpdate(): void {
    this._updateFromControls();
    this._updateGroundDetection();
    //move our mesh
    this.mesh.moveWithCollisions(this._moveDirection);
  }

  public activatePlayerCamera(): UniversalCamera {
    this.scene.registerBeforeRender(() => {
      this._beforeRenderUpdate();
      this._updateCamera();
    });
    return this.camera;
  }

  private _updateCamera(): void {
    let centerPlayer = this.mesh.position.y + 2;
    this._camRoot.position = Vector3.Lerp(
      this._camRoot.position,
      new Vector3(this.mesh.position.x, centerPlayer, this.mesh.position.z),
      0.4
    );
  }

  //--GROUND DETECTION--
  //Send raycast to the floor to detect if there are any hits with meshes below the character
  private _floorRaycast(
    offsetx: number,
    offsetz: number,
    raycastlen: number
  ): Vector3 {
    //position the raycast from bottom center of mesh
    let raycastFloorPos = new Vector3(
      this.mesh.position.x + offsetx,
      this.mesh.position.y + 0.5,
      this.mesh.position.z + offsetz
    );
    let ray = new Ray(raycastFloorPos, Vector3.Up().scale(-1), raycastlen);

    //defined which type of meshes should be pickable
    let predicate = function (mesh) {
      return mesh.isPickable && mesh.isEnabled();
    };

    let pick = this.scene.pickWithRay(ray, predicate);

    if (pick.hit) {
      //grounded
      return pick.pickedPoint;
    } else {
      //not grounded
      return Vector3.Zero();
    }
  }

  //raycast from the center of the player to check for whether player is grounded
  private _isGrounded(): boolean {
    if (this._floorRaycast(0, 0, 0.6).equals(Vector3.Zero())) {
      // console.log("Player is not grounded");
      return false;
    } else {
      // console.log("Player is grounded");
      return true;
    }
  }

  private _updateGroundDetection(): void {
    this._deltaTime = this.scene.getEngine().getDeltaTime() / 1000.0;

    //if not grounded
    if (!this._isGrounded()) {
      //keep applying gravity
      this._gravity = this._gravity.addInPlace(
        Vector3.Up().scale(this._deltaTime * Player.GRAVITY)
      );
      this._grounded = false;
    }
    //limit the speed of gravity to the negative of the jump power
    if (this._gravity.y < -Player.JUMP_FORCE) {
      this._gravity.y = -Player.JUMP_FORCE;
    }

    //update our movement to account for jumping
    this.mesh.moveWithCollisions(this._moveDirection.addInPlace(this._gravity));

    if (this._isGrounded()) {
      this._gravity.y = 0;
      this._grounded = true;
      //keep track of last known ground position
      this._lastGroundPos.copyFrom(this.mesh.position);

      this._jumpCount = 1;
    }

    //Jump detection
    if (this._input.jumpKeyDown && this._jumpCount > 0) {
      this._gravity.y = Player.JUMP_FORCE;
      this._jumpCount--;
    }
  }

  private _updateFromControls(): void {
    this._moveDirection = Vector3.Zero(); // vector that holds movement information
    this._h = this._input.horizontal; //x-axis
    this._v = this._input.vertical; //z-axis

    //check if there is movement to determine if rotation is needed
    let input = new Vector3(
      this._input.horizontalAxis,
      0,
      this._input.verticalAxis
    ); //along which axis is the direction
    if (input.length() == 0) {
      //if there's no input detected, prevent rotation and keep player in same rotation
      return;
    }

    //rotation based on input & the camera angle
    let angle = Math.atan2(
      this._input.horizontalAxis,
      this._input.verticalAxis
    );
    angle += this._camRoot.rotation.y;
    let targ = Quaternion.FromEulerAngles(0, angle, 0);

    this.mesh.rotationQuaternion = Quaternion.Slerp(
      targ,
      targ,
      10 * this._deltaTime
    );

    //--MOVEMENTS BASED ON CAMERA (as it rotates)--
    let fwd = this._camRoot.forward;
    let right = this._camRoot.right;
    let correctedVertical = fwd.scaleInPlace(this._v);
    let correctedHorizontal = right.scaleInPlace(this._h);

    //movement based off of camera's view
    let move = correctedHorizontal.addInPlace(correctedVertical);

    //clear y so that the character doesnt fly up, normalize for next step
    this._moveDirection = new Vector3(
      move.normalize().x,
      0,
      move.normalize().z
    );

    //clamp the input value so that diagonal movement isn't twice as fast
    let inputMag = Math.abs(this._h) + Math.abs(this._v);
    if (inputMag < 0) {
      this._inputAmt = 0;
    } else if (inputMag > 1) {
      this._inputAmt = 1;
    } else {
      this._inputAmt = inputMag;
    }
    //final movement that takes into consideration the inputs
    this._moveDirection = this._moveDirection.scaleInPlace(
      this._inputAmt * Player.PLAYER_SPEED
    );
  }
}
